# USBHelperLauncher

*Wii U USB Helper Launcher is a proxy that allows you to use any Wii U USB Helper version by intercepting network requests to its servers.*


## TUTO - Workaround Wii U USB Helper after its discontinuation

**Note:** As you may already know, Wii U USB Helper has been discontinued, however, this shouldn't mean we're not allowed to use it anymore.

### **Here's how to set it up:**

**Step 1 -** Download latest launcher release: https://github.com/FailedShack/USBHelperLauncher/releases/latest (or https://github.com/Natizyskunk/USBHelperLauncher/releases/latest)

**Step 2 -** Download a compatible version of Wii U USB Helper.
- Wii U USB Helper 0.6.1.655: https://mega.nz/#!rnYSmAzC!KRMwIwtAKk7W565iEZ4YvWbUg8E52K98s6LSxKgD0nY
- Wii U USB Helper 0.6.1.653: https://mega.nz/#!pNYVESxD!fxn0dS4hSihstG-OOJz9gaUJCS5UmVPimmMBED7-POs
- Wii U USB Helper 0.6.1.616 (RECOMMENDED): https://drive.google.com/file/d/1q9936I5Aq7i7rXzyYWIH9NTEtc4VNOjl/view?usp=sharing
- Wii U USB Helper 0.6.1.596: https://mega.nz/#!EFZwEDrD!fnB9P7G4Cxr24BXey4UDT8JnDbfNVzZeUJZyct3N6BQ
- View all preserved versions: https://docs.google.com/spreadsheets/d/1oA78Rf4pPFsEsjIlxCpYflPuAxgjr4dLA64rXBZvUq4/edit?usp=sharing

**Step 3 -** Extract Wii U USB Helper, then extract the launcher in the same folder.

**Step 4 -** Download MakeHostsJson.exe: https://cdn.discordapp.com/attachments/444257892531699762/504730683499872276/MakeHostsJson.exe

**Step 5 -** Extract MakeHostsJson.exe to the same folder, run it and follow the instructions.

**Step 6 -** Download Fix_titlekeys.bat: https://mega.nz/#!z6JCyKhL!fIA_SWZzbPCcF8EgBgac_3XyrtUjU25aHgbG2JLcBqA

**Step 7 -** Run Fix_titlekeys.bat as Administrator.

**Step 8 -** Download Clear_AppData.bat: https://mega.nz/#!jrJAkYqZ!EDV2lV9GlaE4DbM1s40gNcVrLTux6dIgqEbaS8-Nkk8

**Step 9 -** Run Clear_AppData.bat as Administrator.

**Step 10 -** Run USBHelperLauncher.exe and follow the on-screen instructions. Then make a shortcut of it on the desktop and you're all set!

## INFOS

- Video guide: https://youtu.be/FY-OrvFOO9Y
- Old URL to TitleKeys: https://wiiu.titlekeys.gq/
- New URL to TitleKeys: http://wiiutitlekeys.ddns.net/ or http://104.129.56.137/
